from django.contrib import admin
from receipts.models import ExpenseCategory, Account, Receipt

#admin.site.register(ExpenseCategory)
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = ("name","owner", "id")
admin.site.register(Account)
admin.site.register(Receipt)